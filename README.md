
## Execution

### Follow below steps to test the Labseq sequence API endpoint.

1. Download/clone the repository 
2. Navigate to the root of the project folder using a terminal.
3. Generate jar

```bash
  mvn package
```

4. Run jar

```bash
  java -jar target/labseq-0.0.1-SNAPSHOT.jar
```    

5. Test the endpoint in browser

```bash
  http://localhost:8080/swagger-ui/index.html
  
  or
  
  http://localhost:8080/labseq/{number}
```
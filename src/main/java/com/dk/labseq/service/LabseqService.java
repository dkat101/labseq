package com.dk.labseq.service;

import java.math.BigInteger;

public interface LabseqService {

    BigInteger getNthNumber(int n);

}

package com.dk.labseq.service.impl;

import com.dk.labseq.service.LabseqService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;

import java.math.BigInteger;

@Service
@Slf4j
public class LabseqServiceImpl implements LabseqService {

    @Override
    @Cacheable(value = "sequence", key = "#n")
    public BigInteger getNthNumber(int n) {
        log.info("Inside getNthNumber of LabseqServiceImpl");

        if(n < 0) {
            return BigInteger.valueOf(n);
        }
        // Since n is an index, we add 1 for the array size
        // Since the definition has 4 default values, we add 4 for the array size
        BigInteger[] sequence = new BigInteger[n + 1 + 4];

        // Initialize array with the first 4 elements according to array
        sequence[0] = BigInteger.valueOf(0);
        sequence[1] = BigInteger.valueOf(1l);
        sequence[2] = BigInteger.valueOf(0l);
        sequence[3] = BigInteger.valueOf(1l);

        if(n < 4) {
            return sequence[n];
        }

        for(int i = 4; i <= n; i++) {
            sequence[i] = sequence[i - 4].add(sequence[i - 3]);
            System.out.println("index " + i + " - " + sequence[i]);
        }

        return sequence[n];
    }
}

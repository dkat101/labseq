package com.dk.labseq.controller;

import com.dk.labseq.service.LabseqService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;

import java.math.BigInteger;

@RestController
@RequestMapping("/labseq")
@Slf4j
public class LabseqController {

    private LabseqService labseqService;

    public LabseqController(LabseqService labseqService) {
        this.labseqService = labseqService;
    }

    @GetMapping("/{n}")
    public BigInteger getNthNumber(@PathVariable("n") int n) {
        log.info("Inside getNthNumber of LabseqController");
        return labseqService.getNthNumber(n);
    }

}
